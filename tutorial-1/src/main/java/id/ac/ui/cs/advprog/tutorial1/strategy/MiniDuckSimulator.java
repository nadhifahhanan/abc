package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MiniDuckSimulator {

    public class MiniDuckSimulator {
 
    public static void main(String[] args) {
 
        MallardDuck mallard = new MallardDuck();
        RubberDuck  rubberDuckie = new RubberDuck();
        DecoyDuck   decoy = new DecoyDuck();
 
        Duck     model = new ModelDuck();

        mallard.performQuack();
        rubberDuckie.performQuack();
        decoy.performQuack();
   
        model.performFly(); 
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
    }
}
